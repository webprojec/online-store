<?php
//toolbox function
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

//abstract class
abstract class Product
{
    protected $sku;
    protected $name;
    protected $price;
    protected $product_type;
    protected $connection;

    //constructor & destructor
    function __construct()
    {
        $servername = "localhost";
        $username = "id18447668_root";
        $password = "Rokurot@M@k@be-1958";
        $db_name = "id18447668_scandiweb";

        $this->connection = new mysqli($servername, $username, $password, $db_name);
        if ($this
            ->connection
            ->connect_errno)
        {
            die("Connection failed: " . $this
                ->$connection->connect_error);
        }
        $isThereError = test_input($_POST['scancode']);
        if ($isThereError == 1)
        {
            echo "error";
            die();
        }
    }
    function __destruct()
    {
        $this
            ->connection
            ->close();
    }
    public function init_common_vars()
    {
        $this->sku = test_input($_POST["sku"]);
        $this->name = test_input($_POST["name"]);
        $this->price = test_input($_POST["price"]);
        $this->product_type = test_input($_POST["product_type"]);
    }
    //class methods
    public function addProduct()
    {
        $specific_attributes = $this->init_product_vars();
        $product_specific = $specific_attributes[0];
        $height = $specific_attributes[1];
        $width = $specific_attributes[2];
        $length = $specific_attributes[3];

        $query = "INSERT INTO product_list VALUES ('$this->sku', '$this->name', $this->price, '$this->product_type', $product_specific, $height, $width, $length);";
        $mysqlResponse = $this
            ->connection
            ->query($query);
        if ($mysqlResponse === true)
        {
            echo "success";
            die();
        }
        else
        {
            echo "Error: <br>" . $this
                ->connection->error;
            die();
        }
    }
    public function removeProducts()
    {
        $arrayChecked = $_POST;

        $query = "DELETE FROM product_list WHERE";
        $concatString = "";
        $endOfArray = end($arrayChecked);

        foreach ($arrayChecked as $item)
        {
            if ($item != $endOfArray)
            {
                $concatString .= " Sku='$item' OR";
            }
            else
            {
                $concatString .= " Sku='$item'";
            }
        }
        $query .= $concatString;
        $mysqlResponse = $this
            ->connection
            ->query($query);
        if ($mysqlResponse == 1)
        {
            echo "success";
            die();
        }
        else
        {
            echo "Error: <br>" . $this
                ->connection->error;
            die();
        }
    }
}

//derived classes
class DVD_Disk extends Product
{
    protected $size;

    function __construct()
    {
        parent::__construct();
        $this->init_common_vars();
        $this->size = test_input($_POST["size"]);
    }
    function init_product_vars()
    {
        $varArray = array(
            $this->size,
            0,
            0,
            0
        );
        return $varArray;
    }
}
class Book extends Product
{
    protected $weight;

    function __construct()
    {
        parent::__construct();
        $this->init_common_vars();
        $this->weight = test_input($_POST["weight"]);
    }
    function init_product_vars()
    {
        $varArray = array(
            $this->weight,
            0,
            0,
            0
        );
        return $varArray;
    }
}
class Furniture extends Product
{
    protected $height;
    protected $width;
    protected $length;

    function __construct()
    {
        parent::__construct();
        $this->init_common_vars();
        $this->height = test_input($_POST["height"]);
        $this->width = test_input($_POST["width"]);
        $this->length = test_input($_POST["length"]);
    }
    function init_product_vars()
    {
        $varArray = array(
            0,
            $this->height,
            $this->width,
            $this->length
        );
        return $varArray;
    }
}
class Ghost_product extends Product
{
    function __construct()
    {
        parent::__construct();
        $this->sku = "";
        $this->name = "";
        $this->price = 0;
        $this->product_type = '';

    }
    //empty class

}

//decide on product type
function manageProductType($letter)
{
    $letter = "object_" . $letter;
    return $letter();
}
function object_d()
{
    return new Furniture();
}
function object_s()
{
    return new DVD_Disk();
}
function object_w()
{
    return new Book();
}

//flow managing method
function flow()
{
    $request_type = test_input($_POST["requestType"]);

    if ($request_type == 'add')
    {
        $productIdentifier = test_input($_POST["product_type"]);
        $clientProduct = manageProductType($productIdentifier);
        $clientProduct->addProduct();
    }
    else
    {
        if ($request_type == 'delete')
        {
            $clientProduct = new Ghost_product();
            $clientProduct->removeProducts();
        }
        else
        {
            echo "Something went wrong";
            die();
        }
    }
}

//start the flow
flow();
?>
