<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../styles/styles.css"/>
    <link rel="stylesheet" type="text/css" href="../styles/checkbox.css"/>
    <script type="text/javascript" src="../jquery/v3.5.1/jquery.js"></script>
    <script type="text/javascript" src="../js/functionality.js"></script>
    <title>Product List</title>
    <?php
      $servername = "localhost";
        $username = "id18447668_root";
        $password = "Rokurot@M@k@be-1958";
        $db_name = "id18447668_scandiweb";
      // Create connection
      $conn = new mysqli($servername, $username, $password, $db_name);
      // Check connection
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
    ?>
  </head>
  <body>
    <div class="main">
      <div class="top">
        <div class="page-name">
          <h1>Product List</h1>
        </div>

        <div class="navigation">
          <div class="add">
            <form class="add-product" action="../product/add" method="post">
              <button type="submit" name="button">Add</button>
            </form>
          </div>

          <div class="mass-delete">
            <button type="button" name="button" onclick="massDelete()">Mass Delete</button>
          </div>

        </div>
      </div>
      <div class="center">    <!-- CENTER -->
        <?php
          $get_table = $conn -> query("SELECT * FROM product_list");
          while($row = mysqli_fetch_row($get_table))
          {
            echo "<div class='product'>";
              echo "<div class='box'>";
                echo "<div class='box-icon'>";
                  echo "<label class='container'>";
                  echo "<input class='check' type='checkbox' name='$row[0]'>";
                  echo "<span class='checkmark'></span>";
                  echo "</label>";
                echo "</div>";
              echo "</div>";
              echo "<div class='product-details'>";
                echo "<div id='$row[0]' class='sku'>$row[0]</div>";
                echo "<div class='name'>$row[1]</div>";
                echo "<div class='price'>$row[2] $</div>";
                if($row[3] == 's'){
                  echo "<div class='product-specific'>Size: $row[4] MB</div>";
                }
                if($row[3] == 'w'){
                  echo "<div class='product-specific'>Weight: $row[4]KG</div>";
                }
                if($row[3] == 'd'){
                  echo "<div class='product-specific'>Dimensions: $row[5]x$row[6]x$row[7]</div>";
                }

              echo "</div>";

            echo "</div>";
          }
          $conn->close();
         ?>


      </div>

      <div class="bottom">
        <p>Scandiweb Test assignment</p>
      </div>

    </div>
  </body>
</html>
