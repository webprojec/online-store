<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../styles/styles.css">
    <script type="text/javascript" src="../jquery/v3.5.1/jquery.js"></script>
    <script type="text/javascript" src="../js/functionality.js"></script>
    <title>Add Product</title>
  </head>
  <body>


    <div class="main">
      <div class="top">
        <div class="page-name">
          <h1>Product Add</h1>
        </div>

        <div class="navigation">
          <div class="save">
            <button class="save-button" type="submit" name="button" onclick="addItem()">Save</button>
          </div>
          <form class="" action="../product/list" method="post">
            <div class="cancel">
              <button type="submit" name="button">Cancel</button>
            </div>
          </form>
        </div>
      </div>

      <div class="center-add-page">    <!-- CENTER -->
        <div class="property">
            <div class="property-name">SKU</div>
            <div class="property-value">
              <input id="product_sku" type="text" name="sku" onchange="checkSku(this)">
            </div>
        </div>
        <div id="sku_error" class="error-container">
          <div class="error-message-property">
            <p>Please enter UPPERCASE letters and numbers</p>
          </div>
        </div>

        <div class="property">
            <div class="property-name">Name</div>
            <div class="property-value">
              <input id="product_name" type="text" name="name" value="" onchange="checkIfLetter(this)">
            </div>
        </div>
        <div id="name_error" class="error-container">
          <div class="error-message-property">
            <p>Please enter letters</p>
          </div>
        </div>

        <div class="property">
            <div class="property-name">Price ($)</div>
            <div class="property-value">
              <input id="product_price" type="text" name="price" value="" onchange="checkIfNumber(this)">
            </div>
        </div>
        <div id="price_error" class="error-container">
          <div class="error-message-property">
            <p>Please enter numbers</p>
          </div>
        </div>

        <div class="property">
          <div class="select-label">Type Switcher</div>
          <div class="select-value">
            <select class="select-type" id="product_type" name="product_type" onchange="showSpecific()">
              <option class="placeholder" style="border: 2px solid black;" disabled selected>Type Switcher</option>
              <option value='s'>DVD-disc</option>
              <option value='w'>Book</option>
              <option value='d'>Furniture</option>
            </select>

          </div>
        </div>

        <div id='s' class="specific" style="display:none;">
          <div class="specific-property">
            <div class="specific-name">Size (MB)</div>
            <div class="specific-value">
              <input id="product_specific_size" type="type" name="product_specific_size" value="" onchange="checkIfNumber(this)">
            </div>
          </div>
          <div id="product_specific_size_error" class="error-container">
            <div class="error-message-specific">
              <p>Please enter numbers</p>
            </div>
          </div>

          <div class="specific-detail">Please provide size in mega bytes</div>
        </div>

        <div id='d' class="specific" style="display:none;">
          <div class="specific-property">
            <div class="specific-name">Height (CM)</div>
            <div class="specific-value">
              <input id="product_height" type="text" name="height" value="" onchange="checkIfNumber(this)">
            </div>
          </div>
          <div id="height_error" class="error-container">
            <div class="error-message-specific">
              <p>Please enter numbers</p>
            </div>
          </div>

          <div class="specific-property">
            <div class="specific-name">Width (CM)</div>
            <div class="specific-value">
              <input id="product_width" type="text" name="width" value="" onchange="checkIfNumber(this)">
            </div>
          </div>
          <div id="width_error"class="error-container">
            <div class="error-message-specific">
              <p>Please enter numbers</p>
            </div>
          </div>


          <div class="specific-property">
            <div class="specific-name">Length (CM)</div>
            <div class="specific-value">
              <input id="product_length" type="text" name="length" value="" onchange="checkIfNumber(this)">
            </div>
          </div>
          <div id="length_error" class="error-container">
            <div class="error-message-specific">
              <p>Please enter numbers</p>
            </div>
          </div>

          <div class="specific-detail">Please provide dimensions in HxWxL format</div>
        </div>

        <div id='w' class="specific" style="display:none;">
          <div class="specific-property">
            <div class="specific-name">Weight (KG)</div>
            <div class="specific-value">
              <input id="product_specific_weight" type="text" name="product_specific_weight" value="" onchange="checkIfNumber(this)">
            </div>
          </div>
          <div id="product_specific_weight_error" class="error-container">
            <div class="error-message-specific">
              Please enter numbers
            </div>
          </div>

          <div class="specific-detail">Please provide the weight of the book</div>
        </div>
      </div>
      <input type="hidden" id="scancode" name="scancode" value="">
      <script type="text/javascript">
      </script>
      <div class="bottom">
        <p>Scandiweb Test assignment</p>
      </div>

    </div>
  </body>
</html>
