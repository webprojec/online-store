function showSpecific(){
  let hideIDs = ['s', 'd', 'w'];
  hideIDs.forEach(item => {
    document.getElementById(item).style.display = "none";
  });
  var selectDropdown = document.getElementById('product_type');
  var selectedValue = selectDropdown.value;
  var goingToShow = document.getElementById(selectedValue);
  goingToShow.style.display = "flex";

}

function decideOnCondition(condition, _errorContainer){
  if(condition){
    document.getElementById('scancode').setAttribute('value', 0);
  } else {
    _errorContainer.style.display = "flex";
    document.getElementById('scancode').setAttribute('value', 1);
  }
}

function checkIfNumber(object){
  var value = object.value;
  var _errorContainer = document.getElementById((object.name).concat('_error'));
  _errorContainer.style.display = "none";
  var condition = value && value > 0;
  decideOnCondition(condition, _errorContainer);
}

function checkIfLetter(object){
  let regexLetters = /[A-z ]+$/;
  var value = object.value;
  var _errorContainer = document.getElementById((object.name).concat('_error'));
  _errorContainer.style.display = "none";
  var condition = value && regexLetters.test(value);
  decideOnCondition(condition, _errorContainer);
}

function checkSku(object){
  let regexLetters = /^(?=.*[0-9])(?=.*[A-Z])([0-9A-Z]+)$/;
  var value = object.value;
  $.get( "../product/list.php", function(response) {
    var searchPattern = "<div id='";
    searchPattern = searchPattern.concat(value);
    searchPattern = searchPattern.concat("' class='sku'>");
    searchPattern = searchPattern.concat(value);
    searchPattern = searchPattern.concat("</div>");
    if(response.search(searchPattern)>0){
        alert("Product with such SKU aleady exists");
    }
  });
  var _errorContainer = document.getElementById((object.name).concat('_error'));
  _errorContainer.style.display = "none";
  var condition = value && regexLetters.test(value);
  decideOnCondition(condition, _errorContainer);
}

function addItem() {
  var product_sku = $("#product_sku").val();
  var product_name = $("#product_name").val();
  var price = $("#product_price").val();
  var product_type = $("#product_type").val();
  var product_specific_size = $("#product_specific_size").val();
  var product_specific_weight = $("#product_specific_weight").val();
  var product_height = $("#product_height").val();
  var product_width = $("#product_width").val();
  var product_length = $("#product_length").val();
  var scancode = $("#scancode").val();
  var requestType = 'add';
  $.ajax({
      url: "../oop/object-handler.php",
      type: "POST",
      data: {
        sku: product_sku,
        name: product_name,
        price: price,
        product_type: product_type,
        size: product_specific_size,
        weight: product_specific_weight,
        height: product_height,
        width: product_width,
        length: product_length,
        scancode: scancode,
        requestType:requestType
      }
    }).done(function(){
      location.href = "../product/list";
      return false;
    });
}

function massDelete(){
  var arrayChecked = Array();
  var allCheckboxes = document.querySelectorAll('.check');
  allCheckboxes.forEach((item) => {
    if(item.checked == true){
      arrayChecked.push(item.name);
    }
  });
  var requestType = 'delete';
  $.ajax({
    url: "../oop/object-handler.php",
    type: "POST",
    data: Object.assign({
      requestType: requestType,
      scancode: "0"
    }, arrayChecked)
  }).done(function(){
    location.href = "../product/list";
    return false;
  });
}
